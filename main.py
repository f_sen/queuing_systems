import numpy as np
from queues import Queue,collab,EXP3P
from system import System
import matplotlib.pyplot as plt
from tools import phi1




T=10**6
N=4
arrival_rates=np.array([(N+1)/N**2]*N)
servers = np.array([(N-1)/N**2]*N)
servers[0]= 1
C= np.random.rand(N,N)
queues = [collab(arrival_rates[i],K=N,N=N,C=C,rank=i,arrival_rates=arrival_rates,mu=servers,alpha=1./3) for i in range(N)]
#queues = [EXP3P(arrival_rates[i],K=N,T=T) for i in range(N)]
system = System(queues=queues,K=N,service_rates=servers)

for t in range(T):
    system.round(t)
    if t%1000==0:
        print("t",t)
        system.state_summary(t,1000)

h = np.array(system.queue_history)
for i in range(0,N):
    plt.plot(h[:,i])
plt.show()